#ifndef DEFS_H
#define DEFS_H

#define SECONDS_MAX (18446744073709551615LU)
#define MINUTE (60)
#define HOUR   (3600)
#define DAY    (86400)
#define WEEK   (604800)
#define MONTH  (2592000)  /* 30 days */
#define YEAR   (31536000) /* 365 days */
#define ANSI_LINECLR "\x1b[1K\r"

#ifndef VERSION
#    define VERSION ("<undefined>")
#endif
#ifndef VER_YEAR
#    define VER_YEAR (0)
#endif


#endif
