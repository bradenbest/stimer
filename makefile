include config.mk

stimer: main.o timer.o atoul.o iso_8601.o
	$(CC) $^ -o $@

test: test_iso_8601

test_iso_8601: tiso_8601.o test.o atoul.o
	$(CC) $^ -o $@
	echo "testing iso_8601.c"
	./$@

%.o: %.c
	$(CC) -c $^ -DVERSION=$(VERSION) -DVER_YEAR=$(VER_YEAR) -Wall -pedantic

install: stimer stimer.1
	mv stimer /usr/local/bin
	cp stimer.1 /usr/local/share/man/man1/

uninstall:
	rm /usr/local/bin/stimer
	rm /usr/local/share/man/man1/stimer.1

clean:
	rm stimer test_iso_8601 *.o

.PHONY: clean install uninstall
