#ifndef STIMER_H
#define STIMER_H

#include <stdint.h>

void  timer_run  (uint64_t interval);
int   print_time (char const *prefix_text, time_t);

#endif
