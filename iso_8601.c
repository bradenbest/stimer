#include <stdio.h>
#include <string.h>
#include <time.h>

#include "iso_8601.h"
#include "atoul.h"

enum format_type {
    FMT_INVALID,
    /* absolute formats*/
    FMT_ABS_Y,      /* yyyy */
    FMT_ABS_YM,     /* yyyy-mm */
    FMT_ABS_YMD,    /* yyyy-mm-dd */
    FMT_ABS_YMDHM,  /* yyyy-mm-ddTHH:MM */
    FMT_ABS_YMDHMS, /* yyyy-mm-ddTHH:MM:SS */
    /* relative formats */
    FMT_REL_HM,     /* HH:MM */
    FMT_REL_HMS,    /* HH:MM:SS */
    FMT_END,
};

static int                    char_count      (char const *, char);
static enum format_type const get_format      (char const *);
static struct tm const *      parse_format    (char const *, enum format_type const);
static int                    fmt_is_absolute (enum format_type const);

static void set_year              (struct tm *, char const *);
static void set_month             (struct tm *, char const *);
static void set_day               (struct tm *, char const *);
static void set_minutes_and_hours (struct tm *, char const *, enum format_type const);
static void set_seconds           (struct tm *, char const *);

static void
set_year(struct tm *out, char const *str)
{
    out->tm_year = atoul(str, '-') - 1900;
}

static void
set_month(struct tm *out, char const *str)
{
    str = strchr(str, '-') + 1;
    out->tm_mon = atoul(str, '-') - 1;
}

static void
set_day(struct tm *out, char const *str)
{
    str = strchr(str, '-') + 1;
    str = strchr(str, '-') + 1;
    out->tm_mday = atoul(str, 'T');
}

static void
set_minutes_and_hours(struct tm *out, char const *str, enum format_type const format)
{
    if(format == FMT_ABS_YMDHMS)
        str = strchr(str, 'T') + 1;

    out->tm_hour = atoul(str, ':');
    str = strchr(str, ':') + 1;
    out->tm_min = atoul(str, ':');
}

static void
set_seconds(struct tm *out, char const *str)
{
    str = strchr(str, ':') + 1;
    str = strchr(str, ':') + 1;
    out->tm_sec = atoul(str, '\0');
}

static int
fmt_is_absolute(enum format_type const format)
{
    return format == FMT_ABS_Y ||
           format == FMT_ABS_YM ||
           format == FMT_ABS_YMD ||
           format == FMT_ABS_YMDHM ||
           format == FMT_ABS_YMDHMS;
}

/*
 * [unit test verified v1.4.0]
 * parses string given a format
 *
 * bugfix v1.4.1:
 * + makes sure now is only copied if we're using a relative format
 * + explicitly sets seconds for FMT_REL_HM
 * + explicitly sets hours for formats less precise than FMT_ABS_YMDHM
 * + explicitly sets all implied fields for all formats
 *
 * This is done by converting the format to be FMT_ABS_YMDHMS and checking
 * a couple cases instead of using a long, ugly switch statement.
 *
 * these changes prevent these bugs:
 * - seemingly random second offsets (copied from now's seconds offset
 *     when seconds are unspecified)
 * - unable to implicitly set a timer to go off at midnight if not in
 *     UTC+00:00 timezone (e.g. someone in UTC-06:00 will set a timer for
 *     2020-04 only for it to be set to 2020-03-31T17:00)
 */
static struct tm const *
parse_format(char const *str, enum format_type const format)
{
    static char const * format_strings[FMT_END] = {
        /* fills in missing parts necessary to convert to
         * FMT_ABS_YMDHMS format (bugfix v1.4.1)
         */
        [FMT_ABS_Y]      = "-01-01T00:00:00",
        [FMT_ABS_YM]     = "-01T00:00:00",
        [FMT_ABS_YMD]    = "T00:00:00",
        [FMT_ABS_YMDHM]  = ":00",
        [FMT_ABS_YMDHMS] = "",
    };
    static char full_date[1024]; // Extreme buffer overflow prevention
    static struct tm out;
    time_t now = time(NULL);

    if(format == FMT_INVALID || format >= FMT_END)
        return NULL;

    memset(&out, 0, sizeof out); // set to zero so nothing surprising can happen

    // bugfix v1.4.1
    if(fmt_is_absolute(format)){
        strcpy(full_date, str);
        strcat(full_date, format_strings[format]);
        set_year(&out, full_date);
        set_month(&out, full_date);
        set_day(&out, full_date);
        set_minutes_and_hours(&out, full_date, FMT_ABS_YMDHMS);
        set_seconds(&out, full_date);
    }

    else {
        memcpy(&out, localtime(&now), sizeof out);
        strcpy(full_date, str);

        if(format == FMT_REL_HM)
            strcat(full_date, ":00");

        set_minutes_and_hours(&out, full_date, FMT_REL_HMS);
        set_seconds(&out, full_date);

        if(mktime(&out) < now)
            ++out.tm_mday; // defer to tomorrow
    }

    return &out;
}

/*
 * [unit test verified v1.4.0]
 * returns number of times char appears in string
 */
static int
char_count(char const *str, char ch)
{
    int total = 0;

    if(str == NULL || strchr(str, ch) == NULL)
        return 0;

    while((str = strchr(str, ch)) != NULL){
        ++total;
        ++str;
    }

    return total;
}

/*
 * [unit test verified v1.4.0]
 * figures out which format str fits and returns one of seven different
 * format strings.
 * returns NULL if there is no match
 *
 * This isn't rock solid. Malformed input can be snuck past it and the
 * program will likely crash or produce incorrect output.
 *
 * This function's job is not to baby the user, but to analyze the
 * contents of the input and try to determine what format to use based on
 * the following table:
 *
 * format       -   T   :  constant
 * x            0   0   0  FMT_ABS_Y
 * x:x          0   0   1  FMT_REL_HM
 * x:x:x        0   0   2  FMT_REL_HMS
 * x-x          1   0   0  FMT_ABS_YM
 * x-x-x        2   0   0  FMT_ABS_YMD
 * x-x-xTx:x    2   1   1  FMT_ABS_YMDHM
 * x-x-xTx:x:x  2   1   2  FMT_ABS_YMDHMS
 */
static enum format_type const
get_format(char const *str)
{
    size_t len;

    if(str == NULL)
        return FMT_INVALID;

    len = strlen(str);

    for(int i = 0; i < len; ++i)
        if(strchr("0123456789-:T", str[i]) == NULL) /* make sure every character is in-format */
            return FMT_INVALID;

    if( char_count(str, '-') == 0 &&
        char_count(str, 'T') == 0){
        switch(char_count(str, ':')){
            case 0: return FMT_ABS_Y;
            case 1: return FMT_REL_HM;
            case 2: return FMT_REL_HMS;
        }
    }

    if( char_count(str, '-') == 1 &&
        char_count(str, 'T') == 0 &&
        char_count(str, ':') == 0 )
        return FMT_ABS_YM;

    if(char_count(str, '-') == 2){
        if(char_count(str, 'T') == 0)
            return FMT_ABS_YMD;

        if(char_count(str, 'T') == 1){
            switch(char_count(str, ':')){
                case 1: return FMT_ABS_YMDHM;
                case 2: return FMT_ABS_YMDHMS;
            }
        }
    }

    return FMT_INVALID;
}

/*
 * [unit test verified v1.4.0]
 * takes an ISO 8601 string and converts it to a struct tm
 * returns NULL if unable to parse
 */
struct tm const *
iso_8601_parse_str(char const *str)
{
    enum format_type const format = get_format(str);

    if(format == FMT_INVALID)
        return NULL;

    return parse_format(str, format);
}
