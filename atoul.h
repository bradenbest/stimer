#ifndef ATOUL_H
#define ATOUL_H

#include <stdint.h>

uint64_t atoul(char const *src, char delim);

#endif
