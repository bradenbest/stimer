#ifndef TEST_H
#define TEST_H

/* useful for functions consisting of a single expression */
#define TESTFN(name, expr) \
    int name (void) { return (expr); }

typedef int (*testfn_t)(void);

enum { FAIL, PASS };
enum { PASSED, TOTAL };

struct testnode {
    testfn_t fn;
    char *   description;

    int end;
};

int   run_test  (struct testnode *);
int * run_tests (struct testnode *list);

#endif
