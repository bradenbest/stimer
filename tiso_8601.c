/* test module for iso_8601.c */

#include <stdio.h>

#include "iso_8601.c"
#include "test.h"

TESTFN( t_parse_format_y,
        parse_format("2017", FMT_ABS_Y)->tm_year == 117 )

TESTFN( t_parse_format_ym,
        parse_format("2017-04", FMT_ABS_YM)->tm_mon == 3 )

TESTFN( t_parse_format_ymd,
        parse_format("2017-04-26", FMT_ABS_YMD)->tm_mday == 26 )

TESTFN( t_parse_format_ymdhm,
        parse_format("2017-04-26T15:25", FMT_ABS_YMDHM)->tm_hour == 15 &&
        parse_format("2017-04-26T15:25", FMT_ABS_YMDHM)->tm_min == 25 )

TESTFN( t_parse_format_ymdhms,
        parse_format("2017-04-26T15:25:45", FMT_ABS_YMDHMS)->tm_sec == 45 )

TESTFN( t_parse_format_hm,
        parse_format("15:25", FMT_REL_HM)->tm_hour == 15 &&
        parse_format("15:25", FMT_REL_HM)->tm_min == 25 )

TESTFN( t_parse_format_hms,
        parse_format("15:25:45", FMT_REL_HMS)->tm_sec == 45 )

TESTFN( t_char_count,
        char_count("A:S:D:F",   ':') == 3 &&
        char_count("A:S:D:F:",  ':') == 4 &&
        char_count(":A:S:D:F",  ':') == 4 &&
        char_count(":A:S:D:F:", ':') == 5 &&
        char_count("ASDF",      'F') == 1 &&
        char_count("ASDF",      'A') == 1 &&
        char_count("A",         'A') == 1 &&
        char_count("ASDF",      'G') == 0 )

TESTFN( t_iso_8601_parse_str_invalid,
        iso_8601_parse_str("asdf") == NULL &&
        iso_8601_parse_str("2017T") == NULL &&
        iso_8601_parse_str("2017-04T") == NULL &&
        iso_8601_parse_str("2017-04-26T") == NULL &&
        iso_8601_parse_str("2017-04-26T15") == NULL )

TESTFN( t_iso_8601_parse_str_y,
        iso_8601_parse_str("2017") != NULL )

TESTFN( t_iso_8601_parse_str_ym,
        iso_8601_parse_str("2017-04") != NULL )

TESTFN( t_iso_8601_parse_str_ymd,
        iso_8601_parse_str("2017-04-26") != NULL )

TESTFN( t_iso_8601_parse_str_ymdhm,
        iso_8601_parse_str("2017-04-26T15:25") != NULL )

TESTFN( t_iso_8601_parse_str_ymdhms,
        iso_8601_parse_str("2017-04-26T15:25:45") != NULL )

TESTFN( t_iso_8601_parse_str_hm,
        iso_8601_parse_str("15:25") != NULL )

TESTFN( t_iso_8601_parse_str_hms,
        iso_8601_parse_str("15:25:45") != NULL )

TESTFN( t_static_get_format_invalid,
        get_format("asdf") == FMT_INVALID &&
        get_format("2017T") == FMT_INVALID &&
        get_format("2017-04T") == FMT_INVALID &&
        get_format("2017-04-26T") == FMT_INVALID &&
        get_format("2017-04-26T15") == FMT_INVALID )

TESTFN( t_static_get_format_y,
        get_format("2017") == FMT_ABS_Y )

TESTFN( t_static_get_format_ym,
        get_format("2017-04") == FMT_ABS_YM )

TESTFN( t_static_get_format_ymd,
        get_format("2017-04-26") == FMT_ABS_YMD )

TESTFN( t_static_get_format_ymdhm,
        get_format("2017-04-26T15:25") == FMT_ABS_YMDHM )

TESTFN( t_static_get_format_ymdhms,
        get_format("2017-04-26T15:25:45") == FMT_ABS_YMDHMS )

TESTFN( t_static_get_format_hm,
        get_format("15:25") == FMT_REL_HM )

TESTFN( t_static_get_format_hms,
        get_format("15:25:45") == FMT_REL_HMS )

int
main(void)
{
    struct testnode tests[] = {
        {t_char_count,                 "char_count: several boundary tests"},
        {t_static_get_format_invalid,  "get_format :: FMT_INVALID (asdf, 2017{,-04,-04-26}T, ...T15)"},
        {t_static_get_format_y,        "get_format :: FMT_ABS_Y (2017)"},
        {t_static_get_format_ym,       "get_format :: FMT_ABS_YM (2017-04)"},
        {t_static_get_format_ymd,      "get_format :: FMT_ABS_YMD (2017-04-26)"},
        {t_static_get_format_ymdhm,    "get_format :: FMT_ABS_YMDHM (2017-04-26T15:25)"},
        {t_static_get_format_ymdhms,   "get_format :: FMT_ABS_YMDHMS (2017-04-26T15:25:45)"},
        {t_static_get_format_hm,       "get_format :: FMT_REL_HM (15:25)"},
        {t_static_get_format_hms,      "get_format :: FMT_REL_HMS (15:25:45)"},
        {t_parse_format_y,             "parse_format: FMT_ABS_Y"},
        {t_parse_format_ym,            "parse_format: FMT_ABS_YM"},
        {t_parse_format_ymd,           "parse_format: FMT_ABS_YMD"},
        {t_parse_format_ymdhm,         "parse_format: FMT_ABS_YMDHM"},
        {t_parse_format_ymdhms,        "parse_format: FMT_ABS_YMDHMS"},
        {t_parse_format_hm,            "parse_format: FMT_REL_HM"},
        {t_parse_format_hms,           "parse_format: FMT_REL_HMS"},
        {t_iso_8601_parse_str_invalid, "iso_8601_parse_str: asdf, 2017{,-04,-04-26}T, ...T15"},
        {t_iso_8601_parse_str_y,       "iso_8601_parse_str(\"2017\")"},
        {t_iso_8601_parse_str_ym,      "iso_8601_parse_str(\"2017-04\")"},
        {t_iso_8601_parse_str_ymd,     "iso_8601_parse_str(\"2017-04-21\")"},
        {t_iso_8601_parse_str_ymdhm,   "iso_8601_parse_str(\"2017-04-21T15:25\")"},
        {t_iso_8601_parse_str_ymdhms,  "iso_8601_parse_str(\"2017-04-21T15:25:45\")"},
        {t_iso_8601_parse_str_hm,      "iso_8601_parse_str(\"15:25\")"},
        {t_iso_8601_parse_str_hms,     "iso_8601_parse_str(\"15:25:45\")"},
        {.end = 1},
    };
    int *results = run_tests(tests);

    printf("%i of %i tests passed.\n", results[PASSED], results[TOTAL]);
}
