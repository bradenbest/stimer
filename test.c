/*
 * unit test framework
 * I'll make a git repo for this soon
 * -Braden
 */
#include <stdio.h>
#include <string.h>

#include "test.h"

/*
 * runs a single test
 * returns PASS or FAIL
 */
int
run_test(struct testnode *testnode)
{
    char *resultstr[] = {"FAIL", "PASS"};
    int result;

    printf("Test: %s\n", testnode->description);
    result = testnode->fn();
    printf("%s\n\n", resultstr[result]);

    return result;
}

/*
 * returs array[2] of int
 * {passed tests, total tests}
 */
int *
run_tests(struct testnode *list)
{
    static int result[2];

    memset(result, 0, sizeof (int) * 2);

    for(struct testnode *selnode = list; !selnode->end; ++selnode){
        result[PASSED] += run_test(selnode);
        result[TOTAL]++;
    }

    return result;
}
