# stimer -- a simple timer by Braden Best

This is a simple timer for timing anywhere from 1 second to 585 billion years.

## Build / Install

To build, run `make`

    make

To clean, run

    make clean

To install or uninstall:

    sudo make install
    sudo make uninstall

## Usage

Use `stimer` without any args for usage help

To use, pass in a number of seconds. You can pass either a single number or multiple.

    stimer 127          # Start stimer with  2m 7s
    stimer -m 2 -s 7    # Same, but using the option syntax
    stimer 2m 7s        # Same, but using v1.5 syntax

You can even:

    stimer -d 4 -h 19 -m 16 -s 4    # 4d, 19:16:04
    stimer 4d 19h 16m 4s            # same as above

As this is a more UNIX-y kind of tool, you are expected to combine it with something else to make an "alarm"

    stimer 5m && mplayer alarmsound.wav
