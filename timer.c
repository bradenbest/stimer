#include <stdio.h>
#include <time.h>
#include <stdint.h>

#include "timer.h"
#include "defs.h"

enum print_remaining_time_clear_line {
    PRT_NOCLEAR,
    PRT_CLEAR
};

static void pause                (time_t);
static void print_remaining_time (time_t, int clear_line);

static void
pause(time_t interval)
{
    time_t now;
    time_t future = time(NULL) + interval;

    while((now = time(NULL)) < future)
        ;
}

/*
 * Prints remaining time in the format
 * [[[[[[Yy ]Mm ]Ww ]Dd ]HH:]MM:]SS
 *
 * [!] uses ANSI escape sequence to clear line
 */
static void
print_remaining_time(time_t seconds, int clear_line)
{
    int first_print = 0;
    uint64_t unit;
    static uint64_t unit_values[] = {
        YEAR, MONTH, WEEK, DAY, HOUR, MINUTE, 1, 0
    };
    static char const *formats[] = {
        "%luy ", "%lum ", "%luw ", "%lud ",
        "%02lu:", "%02lu:", "%02lu"
    };

    if(clear_line)
        fputs(ANSI_LINECLR, stdout); /* [!] ANSI escape sequence - clears to beginning of line */

    for(int i = 0; unit_values[i] > 0; ++i){
        unit = seconds / unit_values[i];
        seconds %= unit_values[i];

        if(unit > 0 || first_print == 1){
            printf(formats[i], unit);
            first_print = 1;
        }
    }

    fflush(stdout);
}

/*
 * bugfix v1.4.1:
 * + checks if localtime() returns NULL and informs the caller
 *
 * this amends the following bug:
 * - some large inputs (e.g. 2^63-1) overflow the time and cause
 *     localtime to return NULL. Due to the undefined nature of signed
 *     overflow, this cannot be checked with a simple number comparison
 */
int
print_time(char const *prefix_text, time_t time_raw)
{
    static char const * const weekdays[7] = {
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    static char const * const months[12] = {
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    struct tm *time_tm = localtime(&time_raw);

    // bugfix v1.4.1
    if(time_tm == NULL)
        return 0;

    printf("%s%s %s %i %i at %02i:%02i:%02i\n",
            prefix_text,
            weekdays[time_tm->tm_wday], months[time_tm->tm_mon],
            time_tm->tm_mday, time_tm->tm_year + 1900,
            time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec);

    return 1;
}

void
timer_run(uint64_t interval)
{
    time_t now = time(NULL);
    time_t future = now + (time_t)interval;

    print_time("Current time is ", now);

    if(print_time("Timer set for ", future) == 0){
        printf("stimer: invalid interval\n");
        return;
    }

    fputs("Timer will run for: ", stdout);
    print_remaining_time(future - now, PRT_NOCLEAR);
    putchar('\n');

    while((now = time(NULL)) < future){
        print_remaining_time(future - now, PRT_CLEAR);
        pause(1);
    }
}

