/* There's strtoul, but I want to guarantee that our longs are 64-bit */
/* atoul wasn't a good name then, should've been atou64 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include "atoul.h"

uint64_t
atoul(char const *src, char delim)
{
    uint64_t total = 0;
    char digit;
    int len = 0;

    if(src == NULL)
        return 0;

    while((digit = src[len++]) != delim && isdigit(digit))
        total = (total * 10) + (digit - '0');

    if(len > 19)
        puts("stimer: warning: input may overflow.");

    return total;
}
