#ifndef ISO_8601_H
#define ISO_8601_H

#include <time.h>

struct tm const * iso_8601_parse_str(char const *str);

#endif
