#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include "timer.h"
#include "atoul.h"
#include "defs.h"
#include "iso_8601.h"

typedef void (*flagfn_t)(char *, int32_t);

struct flag {
    char *   flag;        /* e.g. "v" to match -v */
    char *   description; /* for usage() */
    int      has_arg;     /* NO_ARG or HAS_ARG */
    flagfn_t callback;    /* see callback__* functions */
    int32_t  extra_arg;   /* for add_unit; max value = ~31M */

    int end; /**< marks end of struct flag[] array if non-zero */
};

enum { NO_ARG, HAS_ARG };

static void callback__version_info (char *unused, int32_t unused2);
static void callback__usage_info   (char *unused, int32_t unused2);
static void callback__add_unit     (char *value_chr, int32_t unit);
static void callback__use_iso      (char *value_chr, int32_t unused);

static void usage   (void);
static void version (void);

static uint64_t interval;

static struct flag flags[] = {
    {"v", "show version and exit",        NO_ARG,  callback__version_info},
    {"H", "show usage help and exit",     NO_ARG,  callback__usage_info},
    {"s", "add <arg> seconds to timer",   HAS_ARG, callback__add_unit, 1},
    {"m", "add <arg> minutes to timer",   HAS_ARG, callback__add_unit, MINUTE},
    {"h", "add <arg> hours to timer",     HAS_ARG, callback__add_unit, HOUR},
    {"d", "add <arg> days to timer",      HAS_ARG, callback__add_unit, DAY},
    {"w", "add <arg> weeks to timer",     HAS_ARG, callback__add_unit, WEEK},
    {"M", "add <arg> months to timer",    HAS_ARG, callback__add_unit, MONTH},
    {"y", "add <arg> years to timer",     HAS_ARG, callback__add_unit, YEAR},
    {"i", "set timer to <arg> (ISO8601)", HAS_ARG, callback__use_iso},
    {.end = 1}
};

static char const *timeunit_chr = "smhdwMy";

static int timeunit_value[] = {
    1, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR,
};

static void
callback__version_info(char *unused, int32_t unused2)
{
    version();
    exit(1);
}

static void
callback__usage_info(char *unused, int32_t unused2)
{
    usage();
}

static void
callback__add_unit(char *value_chr, int32_t unit)
{
    uint64_t value = atoul(value_chr, '\0');

    interval += value * unit;
}

static void
callback__use_iso(char *value_chr, int32_t unused)
{
    struct tm const *result = iso_8601_parse_str(value_chr);
    time_t now = time(NULL);
    time_t future;

    if(result == NULL){
        printf("stimer: invalid iso 8601 format.\n\n");
        usage();
    }

    future = mktime((struct tm *)result);

    if(future < now){
        printf("stimer: this date is in the past.\n\n");
        usage();
    }

    interval = future - now;
}

static void
usage(void)
{
    static char const * const argstr[] = { "     ", "<arg>" };

    puts("stimer - set a timer");
    version();
    puts("usage: stimer [options] [seconds]");
    printf("seconds: an integer between 0 and %lu\n", SECONDS_MAX);
    puts("seconds may also end in one of [smhdwMy] to denote a specific unit of time");
    puts("options:");

    for(struct flag *selflag = flags; !selflag->end; ++selflag)
        printf("  -%s  %s  %s\n",
                selflag->flag,
                argstr[selflag->has_arg],
                selflag->description);

    puts("See `man stimer` for more information");
    exit(1);
}

static void
version(void)
{
    printf("stimer v%s by Braden Best, 2015-%d. Public Domain. No warranty.\n", VERSION, VER_YEAR);
}

/*
 * returns flag to execute or NULL if no match found
 */
struct flag *
find_flag(char *flag)
{
    for(struct flag *selflag = flags; !selflag->end; ++selflag)
        if(strcmp(selflag->flag, flag) == 0)
            return selflag;

    return NULL;
}

/*
 * takes flag and following argument
 * returns number of args to skip/shift
 * exits with usage if an error occurs
 */
int
parse_flag(char *flag, char *arg)
{
    struct flag *selflag = find_flag(flag);

    if(selflag == NULL){
        printf("stimer: unrecognized flag '-%s'.\n\n", flag);
        usage();
    }

    if(!selflag->has_arg){
        selflag->callback(NULL, 0);
        return 1;
    }

    if(arg == NULL){
        printf("stimer: missing parameter on flag '-%s'.\n\n", flag);
        usage();
    }

    selflag->callback(arg, selflag->extra_arg);
    return 2;
}

int
default_arg_handler(char *arg)
{
    uint64_t basevalue = atoul(arg, '\0');
    char endchr = arg[strlen(arg) - 1];
    char const *match = strchr(timeunit_chr, endchr);

    if(match == NULL)
        return basevalue;

    return basevalue * timeunit_value[match - timeunit_chr];
}

/*
 * parses a list of arguments
 */
void
parse_args(char **args)
{
    while(*args){
        if(**args == '-')
            args += parse_flag((*args) + 1, *(args + 1));

        else
            interval += default_arg_handler(*args++);
    }
}

int
main(int argc, char **argv)
{
    if(argv[1] == NULL)
        usage();

    parse_args(argv + 1);
    timer_run(interval);
    fputs(ANSI_LINECLR, stdout);

    return 0;
}
